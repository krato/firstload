<?php

namespace App\Presenters;

class ChartPresenter extends BasePresenter
{

	/** @var \App\Repositories\UrlRepository @inject */
	public $urlRepository;

	/** @var \App\Repositories\ChecksetRepository @inject */
	public $checksetRepository;

	public function renderDefault($url = '')
	{
		$this->template->urls = $this->urlRepository->findAllVisible();

		$this->template->url = $url;

		if ($url) {
			$this->template->checksets = $this->checksetRepository->findAllByUrl($url);

			$data = [];
			for ($i = 1; $i <= 3; $i++) {
				$data[$i] = [];
				foreach ($this->template->checksets as $checkset) {
					$obj = new \stdClass();
					$obj->x = $checkset->time->getTimestamp() * 1000;
					switch ($i) {
						case 1: $value = $checkset->t1;
							break;
						case 2: $value = $checkset->t2;
							break;
						case 3: $value = $checkset->t3;
							break;
					}
					if ($value === NULL) {
						$value = -1000;
						$info = [];
						if ($checkset->http_status) {
							$info['HTTP CODE'] = $checkset->http_status;
						}
						if ($checkset->curl_errno) {
							$info['CURL CODE'] = $checkset->curl_errno;
						}
						if ($checkset->curl_error) {
							$info['CURL TEXT'] = $checkset->curl_error;
						}
						$text = sprintf('%s', $checkset->time);
						$text .= '<ul>';
						foreach ($info as $key => $t) {
							$text .= sprintf('<li>%s: %s</li>', $key, $t);
						}
						$text .= '</ul>';
						$obj->toolTipContent = $text;
					}
					else {
						if ($value) {
							$text = sprintf('%s: <b>%d ms</b>', $checkset->time, $value);
						}
						$obj->toolTipContent = $text;
					}
					if ($value > 5000) {
						$value = 5000;
					}
					$obj->y = $value;
					$data[$i][] = $obj;
				}
			}
			$this->template->data = $data;
		}
	}

}
