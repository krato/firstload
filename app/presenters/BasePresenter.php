<?php

namespace App\Presenters;

use Nette;

abstract class BasePresenter extends Nette\Application\UI\Presenter
{

	/**
	 * Formats view template file names.
	 * @return array
	 */
	public function formatTemplateFiles()
	{
		$files = parent::formatTemplateFiles();
		$dir = dirname($this->getReflection()->getFileName());
		$files[] = "$dir/$this->view.latte";
		return $files;
	}

	/**
	 * Formats layout template file names.
	 * @return array
	 */
	public function formatLayoutTemplateFiles()
	{
		$files = parent::formatLayoutTemplateFiles();
		$dir = dirname($this->getReflection()->getFileName());
		$files[] = dirname($dir) . '/@layout.latte';
		return $files;
	}

}
