<?php

namespace App\Presenters;

class CheckPresenter extends BasePresenter
{

	/** @var \App\Services\CheckerService @inject */
	public $checkerService;

	public function renderNext()
	{
		$checkset = $this->checkerService->checkNext();
		dump($checkset);
	}

}
