<?php

namespace App\Presenters;

class HomepagePresenter extends BasePresenter
{

	/** @var \App\Repositories\UrlRepository @inject */
	public $urlRepository;

	public function renderDefault()
	{
		$this->template->urls = $this->urlRepository->findAllVisible();
	}

}
