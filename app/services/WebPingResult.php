<?php

namespace App\Services;

/**
 * @author krato
 */
class WebPingResult extends \Nette\Object
{

	public $content;
	public $status;
	public $success;
	public $time; // float in seconds
	public $url;
	public $curlErrno;
	public $curlError;

}
