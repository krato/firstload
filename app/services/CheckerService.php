<?php

namespace App\Services;

/**
 * @author krato
 */
class CheckerService extends \Nette\Object
{

	/** @var \App\Repositories\UrlRepository */
	protected $urlRepository;

	/** @var \App\Repositories\ChecksetRepository */
	protected $checksetRepository;

	/** @var \App\Services\WebPingService */
	protected $webPingService;

	public function __construct(\App\Repositories\UrlRepository $urlRepository, \App\Repositories\ChecksetRepository $checksetRepository, \App\Services\WebPingService $webPingService)
	{
		$this->urlRepository = $urlRepository;
		$this->checksetRepository = $checksetRepository;
		$this->webPingService = $webPingService;
	}

	/**
	 * @return \Nette\Database\Table\IRow|NULL
	 */
	public function checkNext()
	{
		$now = new \Nette\Utils\DateTime();
		$minutes = 30 + rand(0, 30);
		$modifier = sprintf('+ %d minutes', $minutes);
		$url = $this->urlRepository->findNextOne()->fetch();
		if (!$url) {
			return NULL;
		}

		$values = [
			'last_check' => $now,
			'next_check' => $now->modifyClone($modifier),
		];
		$url->update($values);

		$values = [
			'url' => $url->url,
			'time' => $now,
			't1' => NULL,
			't2' => NULL,
			't3' => NULL,
		];
		$errorPing = NULL;
		$ping = $this->webPingService->ping($url);
		if ($ping->success) {
			$values['t1'] = ceil($ping->time * 1000);
		}
		else {
			if (!$errorPing) {
				$errorPing = $ping;
			}
		}
		$status = $ping->status;
		$ping = $this->webPingService->ping($url);
		if ($ping->success) {
			$values['t2'] = ceil($ping->time * 1000);
		}
		else {
			if (!$errorPing) {
				$errorPing = $ping;
			}
		}
		if ($status === 200) {
			$status = $ping->status;
		}
		sleep(15);
		$ping = $this->webPingService->ping($url);
		if ($ping->success) {
			$values['t3'] = ceil($ping->time * 1000);
		}
		else {
			if (!$errorPing) {
				$errorPing = $ping;
			}
		}
		if ($status === 200) {
			$status = $ping->status;
		}
		$values['http_status'] = $status;

		if ($errorPing) {
			$values['curl_errno'] = $ping->curlErrno;
			$values['curl_error'] = $ping->curlError;
		}
		$row = $this->checksetRepository->insert($values);
		return $row;
	}

}
