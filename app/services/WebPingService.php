<?php

namespace App\Services;

/**
 * @author krato
 */
class WebPingService extends \Nette\Object
{

	protected $timeout;

	public function __construct($timeout)
	{
		$this->timeout = $timeout;
	}

	public function ping($url)
	{
		$result = new WebPingResult();
		$result->url = $url;

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
		$tempDir = $GLOBALS['container']->parameters['tempDir'];
		$cookieFile = $tempDir . '/curl_cookies.txt';
		curl_setopt($curl, CURLOPT_COOKIESESSION, true);
		curl_setopt($curl, CURLOPT_COOKIEFILE, $cookieFile);
		curl_setopt($curl, CURLOPT_COOKIEJAR, $cookieFile);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $this->timeout); //timeout in seconds
		curl_setopt($curl, CURLOPT_TIMEOUT, $this->timeout); //timeout in seconds
		$startTime = microtime(TRUE);
		$result->content = curl_exec($curl);
		$endTime = microtime(TRUE);
		$result->time = $endTime - $startTime;
		$result->curlErrno = curl_errno($curl);
		$result->curlError = curl_error($curl);
		$result->status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$result->success = ($result->status == 200);
		curl_close($curl);

		return $result;
	}

}
