-- phpMyAdmin SQL Dump
-- version 4.4.9
-- http://www.phpmyadmin.net
--
-- Počítač: localhost
-- Vytvořeno: Čtv 18. úno 2016, 16:27
-- Verze serveru: 5.5.47-0ubuntu0.14.04.1
-- Verze PHP: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Databáze: `c_firstload`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `checkset`
--

CREATE TABLE IF NOT EXISTS `checkset` (
  `url` varchar(255) NOT NULL,
  `time` datetime NOT NULL,
  `t1` int(11) DEFAULT NULL COMMENT '[ms]',
  `t2` int(11) DEFAULT NULL COMMENT '[ms]',
  `t3` int(11) DEFAULT NULL COMMENT '[ms]',
  `http_status` int(11) DEFAULT NULL,
  `curl_errno` smallint(6) DEFAULT NULL,
  `curl_error` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabulky `url`
--

CREATE TABLE IF NOT EXISTS `url` (
  `url` varchar(255) NOT NULL,
  `enabled` smallint(6) NOT NULL DEFAULT '1',
  `last_check` datetime DEFAULT NULL,
  `next_check` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `checkset`
--
ALTER TABLE `checkset`
  ADD PRIMARY KEY (`time`),
  ADD KEY `url` (`url`);

--
-- Klíče pro tabulku `url`
--
ALTER TABLE `url`
  ADD PRIMARY KEY (`url`);

--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `checkset`
--
ALTER TABLE `checkset`
  ADD CONSTRAINT `checkset_ibfk_1` FOREIGN KEY (`url`) REFERENCES `url` (`url`) ON DELETE CASCADE ON UPDATE CASCADE;
