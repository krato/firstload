<?php

namespace App\Repositories;

class ChecksetRepository extends BaseDatabaseRepository
{
    public function findAllByUrl($url)
	{
		return $this->findAll()->where('url', $url)->order('time');
	}

}
