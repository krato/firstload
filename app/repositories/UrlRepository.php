<?php

namespace App\Repositories;

class UrlRepository extends BaseDatabaseRepository
{

	public function findAllVisible()
	{
		return parent::findAll()->where('enabled = 1')->order('url');
	}

	/**
	 * @return \Nette\Database\Table\ActiveRow
	 */
	public function findNextOne()
	{
		$row = $this
				->findAllVisible()
				->where('next_check IS NULL OR next_check <= NOW()')
				->order('next_check')
				->limit(1);
		return $row;
	}

}
