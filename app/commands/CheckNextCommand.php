<?php

namespace App\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author krato
 */
class CheckNextCommand extends Command
{

	/** @var \App\Services\CheckerService */
	protected $checkerService;

	public function __construct(\App\Services\CheckerService $checkerService)
	{
		parent::__construct();
		$this->checkerService = $checkerService;
	}

	protected function configure()
	{
		$this->setName('check:next')
				->setDescription('Checks next URL in queue');
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$this->checkerService->checkNext();
	}

}
